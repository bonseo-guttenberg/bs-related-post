# bs-related-post

Un módulo Guttenberg para mostrar en pantalla los artículos relacionados con una página o post. La relaciones pueden ser configurables a distintos niveles:

Nivel relacional:
0 -> Últimos Post Globales
1 -> Categoría
2 -> Tag
3 -> Slug